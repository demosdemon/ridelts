//
//  LTSOTViaDelegate.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LTSOTViaDelegate <NSObject>

- (void) updateVehicle:(NSDictionary *)vehicle;

- (void) updateShelter:(NSDictionary *)shelter;

- (void) updateRoute:(NSDictionary *)route;

- (void) parseKML:(NSData *)data forRoute:(NSNumber *)route;

@end
