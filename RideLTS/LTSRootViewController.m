//
//  LTSRootViewController.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "LTSAppDelegate.h"
#import "LTSRootViewController.h"
#import "LTSOTVia.h"

#import "LTSVehicleView.h"
#import "LTSRouteOverlay.h"

#import "LTSTrackingButton.h"

@interface LTSRootViewController () {
  @private
    NSTimer * _vehicleTimer;
    NSTimer * _shelterTimer;
}

@end

#define kNavBarButtonImage [UIImage imageNamed:@"NavBarIconLauncher.png"]

#define kMapCenterPoint {30.223889, -92.019722}
static const MKCoordinateRegion kDefaultRegion = {kMapCenterPoint, MKCoordinateSpanForMiles(5.0)};
static const NSTimeInterval kVehicleTimerFrequency = 1.0;
static const NSTimeInterval kShelterTimerFrequency = 5.0;

@implementation LTSRootViewController

@synthesize otvia = _otvia;

@synthesize Routes = _Routes;
@synthesize RouteLayers = _RouteLayers;
@synthesize Vehicles = _Vehicles;
@synthesize Shelters = _Shelters;
@synthesize SelectedRoutes = _SelectedRoutes;

@synthesize mapView = _mapView;

#pragma mark - Initializers

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _otvia = [[LTSOTVia alloc] init];
        _otvia.delegate = self;

        _Routes = [NSMutableArray array];
        _RouteLayers = [NSMutableArray array];
        _Vehicles = [NSMutableArray array];
        _Shelters = [NSMutableArray array];
        _SelectedRoutes = [NSArray array];
    }
    return self;
}

- (void) dealloc {
    [_Vehicles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        //        [obj removeObserver:self forKeyPath:@"coordinate"];
    }];
    [self stopTimers];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:kNavBarButtonImage
//                                                                             style:UIBarButtonItemStyleDone
//                                                                            target:self
//                                                                            action:@selector(revealLeftSidebar:)];
//
//    self.navigationItem.revealSidebarDelegate = self;

    self.mapSubview = [[UIView alloc] initWithFrame:self.view.bounds];
    self.mapSubview.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

    self.mapView = [[MKMapView alloc] initWithFrame:self.mapSubview.bounds];
    self.mapView.delegate = self;
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.mapSubview addSubview:self.mapView];

    self.trackingButton = [[LTSTrackingButton alloc] initWithMapView:self.mapView];
    [self.mapSubview addSubview:self.trackingButton];
    [self setTrackingButtonFrame];
    [self.view addSubview:self.mapSubview];

    //MKUserTrackingBarButtonItem * locButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    //self.navigationItem.rightBarButtonItem = locButton;


    [self startTimers];

    [NSTimer scheduledTimerWithTimeInterval:1.0 block:^{
        [self.mapView setRegion:[self.mapView regionThatFits:kDefaultRegion] animated:YES];
    } repeats:NO];
    
    [self.otvia requestRoutes];
}

#pragma mark - RVC Methods

- (void) startTimers {
    if (!_vehicleTimer)
        _vehicleTimer = [NSTimer scheduledTimerWithTimeInterval:kVehicleTimerFrequency
                                                         target:self
                                                       selector:@selector(fireVehicleTimer:)
                                                       userInfo:nil repeats:YES];
    if (!_shelterTimer)
        _shelterTimer = [NSTimer scheduledTimerWithTimeInterval:kVehicleTimerFrequency
                                                         target:self
                                                       selector:@selector(fireShelterTimer:)
                                                       userInfo:nil repeats:YES];

    [self.otvia startTimers];
}

- (void) stopTimers {
    if (_vehicleTimer) {
        [_vehicleTimer invalidate];
        _vehicleTimer = nil;
    }
    if (_shelterTimer) {
        [_shelterTimer invalidate];
        _shelterTimer = nil;
    }

    [self.otvia stopTimers];
}

- (NSArray *) getRoutesForRequest {
    if (self.Routes.count == 0) return [NSArray array];

    NSArray * requestRoutes = NSNotNilNorEmpty(self.SelectedRoutes) ? self.SelectedRoutes : self.Routes;
    __block NSMutableArray * requestIds = [NSMutableArray arrayWithCapacity:requestRoutes.count];
    [requestRoutes enumerateObjectsUsingBlock:^(LTSRouteOverlay * obj, NSUInteger idx, BOOL *stop) {
        [requestIds insertObject:obj.RouteID atIndex:idx];
    }];

    return [NSArray arrayWithArray:requestIds];
}

- (void) setTrackingButtonFrame {
    [self setTrackingButtonFrameWithDuration:0];
}

- (void) setTrackingButtonFrameWithDuration:(NSTimeInterval)duration {
    void(^animateBlock)(void) = ^{
        self.trackingButton.frame = (CGRect){{5, self.mapSubview.bounds.size.height - self.trackingButton.bounds.size.height - 5},
            self.trackingButton.bounds.size};
    };

    if (duration)
        [UIView animateWithDuration:duration animations:animateBlock];
    else
        animateBlock();
    
}

//- (void)revealLeftSidebar:(id)sender {
//    [self.navigationController toggleRevealState:JTRevealedStateLeft ];
//}

#pragma end

#pragma mark - NSTimer Methods

- (void) fireVehicleTimer:(NSTimer *)timer {
    NSArray * requestRoutes = [self getRoutesForRequest];
    if (NSNilOrEmpty(requestRoutes)) return;

    [self.otvia requestVehiclesForRoutes:requestRoutes];
}

- (void) fireShelterTimer:(NSTimer *)timer {
    return;
    NSArray * requestRoutes = [self getRoutesForRequest];
    if (NSNilOrEmpty(requestRoutes)) return;

    [self.otvia requestSheltersForRoutes:requestRoutes];
}

#pragma mark - LTSOTViaDelegate Methods

- (void) updateVehicle:(NSDictionary *)vehicle {
    if (!vehicle) return;

    LTSVehicleAnnotation * annotation = [LTSVehicleAnnotation vehicleWithDictionary:vehicle];
    if (![self.Vehicles containsObject:annotation]) {
        [self.Vehicles addObject:annotation];
    }

    //if (annotation.OOS) return;
    if ([self.mapView.annotations containsObject:annotation]) return;
    [self.mapView addAnnotation:annotation];
}

- (void) updateShelter:(NSDictionary *)shelter {

}

- (void) updateRoute:(NSDictionary *)route {
    if (!route) return;

    LTSRouteOverlay * overlay = [LTSRouteOverlay routeWithDictionary:route];
    if ([self.Routes containsObject:overlay]) return;

    [self.Routes addObject:overlay];
    [self.otvia requestKMLForRoute:overlay.RouteID];
}

- (void) parseKML:(NSData *)data forRoute:(NSNumber *)route {
    LTSRouteOverlay * overlay = [LTSRouteOverlay routeWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:route, @"RouteID", nil]];
    [overlay parseKML:data];

    [self.mapView addOverlays:overlay.overlays];
}

#pragma mark - MKMapViewDelegate Methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if (annotation == mapView.userLocation) return nil;

    MKAnnotationView * view;
    if ([annotation isKindOfClass:[LTSVehicleAnnotation class]]) {
        view = [mapView dequeueReusableAnnotationViewWithIdentifier:[LTSVehicleView ReuseIdentifier]];
        if (!view)
            view = [LTSVehicleView viewWithAnnotation:annotation mapView:self.mapView];
    }

    if (!view) {
        static NSString * const DefaultReuseIdentifier = @"DefaultReuseIdentifier";
        view = [mapView dequeueReusableAnnotationViewWithIdentifier:DefaultReuseIdentifier];
        if (!view)
            view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:DefaultReuseIdentifier];
    }

    return view;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay {
    for (LTSRouteOverlay * route in self.Routes) {
        MKOverlayView * view = [route viewForOverlay:overlay];
        if (view) return view;
    }
    return nil;
}

- (void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    MKAnnotationView * locView = [mapView viewForAnnotation:userLocation];
    NSLog(@"Loc View class = %@", NSStringFromClass([locView class]));
    
}

- (void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view.annotation isKindOfClass:[MKUserLocation class]]) return;

    if ([view isKindOfClass:[LTSVehicleAnnotation class]]) {
        //
    }
}

#pragma mark - UIView Methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillLayoutSubviews {
    [self setTrackingButtonFrame];
}


- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self setTrackingButtonFrameWithDuration:duration];

    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

#pragma mark - KVO Methods

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([@"coordinate" caseInsensitiveCompare:keyPath] == NSOrderedSame &&
        [[change objectForKey:NSKeyValueChangeNotificationIsPriorKey] boolValue] &&
        [self.Vehicles containsObject:object] &&
        [self.mapView.annotations containsObject:object]) {
        [self.mapView removeAnnotation:object];
    }
    
    //[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}


@end
