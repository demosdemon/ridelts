//
//  LTSTrackingButton.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/10/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "LTSTrackingButton.h"

#define UIImageTrackingModeNone [UIImage imageNamed:@"button_my_location.png"]
#define UIImageTrackingModeFollow [UIImage imageNamed:@"button_my_location_centered.png"]
#define UIImageTrackingModeFollowWithHeading [UIImage imageNamed:@"button_my_location_compass_mode.png"]

@interface LTSTrackingButton () 
@end

@implementation LTSTrackingButton

@synthesize MapView = _MapView;
@synthesize TrackingMode = _TrackingMode;

- (id)initWithMapView:(MKMapView *)mapView {
    self = [super initWithImage:UIImageTrackingModeNone];
    if (self) {
        _TrackingMode = -1;
        self.MapView = mapView;
        [self setUserInteractionEnabled:YES];
        //tapEventStarted = NO;
        self.backgroundColor = [UIColor clearColor];

        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)]];
    }
    return self;
}

- (void) setMapView:(MKMapView *)MapView {
    if (_MapView)
        [_MapView removeObserver:self forKeyPath:@"userTrackingMode"];
    [self willChangeValueForKey:@"MapView"];
    _MapView = MapView;
    [self didChangeValueForKey:@"MapView"];
    [_MapView addObserver:self forKeyPath:@"userTrackingMode" options:(NSKeyValueObservingOptionNew + NSKeyValueObservingOptionInitial) context:NULL];
}

- (void) setTrackingMode:(MKUserTrackingMode) TrackingMode {
    TrackingMode %= 3;
    if (_TrackingMode == TrackingMode && self.MapView.userTrackingMode == TrackingMode) return;
    [self willChangeValueForKey:@"TrackingMode"];
    _TrackingMode = TrackingMode;
    [self didChangeValueForKey:@"TrackingMode"];
    [self.MapView setUserTrackingMode:TrackingMode animated:YES];

    switch (_TrackingMode) {
        case MKUserTrackingModeNone:
            self.image = UIImageTrackingModeNone;
            break;
        case MKUserTrackingModeFollow:
            self.MapView.showsUserLocation = YES;
            self.image = UIImageTrackingModeFollow;
            break;
        case MKUserTrackingModeFollowWithHeading:
            self.MapView.showsUserLocation = YES;
            self.image = UIImageTrackingModeFollowWithHeading;
            break;
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.MapView
        && ([@"userTrackingMode" caseInsensitiveCompare:keyPath]==NSOrderedSame)) {
        self.TrackingMode = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
    }

    //[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void) handleTap {
    self.TrackingMode = (self.TrackingMode + 1) % 2;
    if (self.TrackingMode == 0)
        self.MapView.showsUserLocation = NO;
}


//- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (touches.count ==1)
//        tapEventStarted = YES;
//    else
//        [super touchesBegan:touches withEvent:event];
//}
//
//- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (tapEventStarted)
//        tapEventStarted = NO;
//    else
//        [super touchesMoved:touches withEvent:event];
//}
//
//- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (tapEventStarted)
//        tapEventStarted = NO;
//    else
//        [super touchesCancelled:touches withEvent:event];
//}
//
//- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (touches.count == 1 && tapEventStarted) {
//        tapEventStarted = NO;
//        self.TrackingMode += 1;
//    } else
//        [super touchesEnded:touches withEvent:event];
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
