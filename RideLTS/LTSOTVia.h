//
//  RLTSOTVia.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/8/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LTSOTViaDelegate.h"

@interface LTSOTVia : NSObject {
  @private
    NSNumber * _lastVehicleHttpRequestTime;
    NSNumber * _lastShelterHttpRequestTime;
    __weak id<LTSOTViaDelegate> _delegate;

    NSTimer * _statusTimer;
}

@property (nonatomic, weak) id<LTSOTViaDelegate> delegate;

@property (nonatomic, retain, strong) NSDictionary * SelectedRoutes;

- (void) startTimers;
- (void) stopTimers;

- (void) requestRoutes;
- (void) requestKMLForRoute:(NSNumber *)route;
- (void) requestVehiclesForRoutes:(NSArray *)routes;
- (void) requestSheltersForRoutes:(NSArray *)routes;

@end
