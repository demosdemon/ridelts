//
//  LTSRootViewController.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LTSOTViaDelegate.h"

@class LTSTrackingButton;

@class LTSOTVia;

@interface LTSRootViewController : UIViewController
<LTSOTViaDelegate, MKMapViewDelegate>

@property (strong, nonatomic) LTSOTVia * otvia;

@property (strong, nonatomic) NSMutableArray * Routes;
@property (strong, nonatomic) NSMutableArray * RouteLayers;
@property (strong, nonatomic) NSMutableArray * Vehicles;
@property (strong, nonatomic) NSMutableArray * Shelters;
@property (strong, nonatomic) NSArray * SelectedRoutes;

@property (strong, nonatomic) MKMapView * mapView;
@property (strong, nonatomic) UIView * mapSubview;
@property (strong, nonatomic) LTSTrackingButton * trackingButton;

@property (strong, nonatomic) UIView * leftSideBarView;

- (void) startTimers;
- (void) stopTimers;

@end
