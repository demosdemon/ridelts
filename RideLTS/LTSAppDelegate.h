//
//  RLTSAppDelegate.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/8/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LTSRootViewController;

@interface LTSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;
@property (strong, nonatomic) UINavigationController * navController;
@property (strong, nonatomic) LTSRootViewController * rvc;

@end
