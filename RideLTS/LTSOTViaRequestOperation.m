//
//  LTSOTViaRequestOperation.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "LTSOTViaRequestOperation.h"
#import "LTSOTVia.h"

@interface LTSOTViaRequestOperation ()
@property (readwrite, nonatomic, strong) NSArray * responseYAML;
@property (readwrite, nonatomic, strong) NSError * YAMLError;
@property (readwrite, nonatomic, strong) NSRecursiveLock * lock;
@end

@implementation LTSOTViaRequestOperation
@synthesize responseYAML = _responseYAML;
@synthesize YAMLError = _YAMLError;
@dynamic lock;

+ (LTSOTViaRequestOperation *) YAMLRequestOperationWithRequest:(NSURLRequest *)urlRequest
                                                       success:(YAMLRequestSuccessBlock)success
                                                       failure:(YAMLRequestFailureBlock)failure {
    LTSOTViaRequestOperation * operation = [[LTSOTViaRequestOperation alloc] initWithRequest:urlRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success)
            success(operation.request, operation.response, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure)
            failure(operation.request, operation.response, error, [(LTSOTViaRequestOperation *)operation responseYAML]);
    }];
    return operation;
}


+ (BOOL) canProcessRequest:(NSURLRequest *)urlRequest {
    return [urlRequest.URL.host caseInsensitiveCompare:[BASE_URL host]] == NSOrderedSame;
}

- (NSArray *) responseYAML {
    [self.lock lock];
    if (!_responseYAML && [self.responseData length] > 0 && [self isFinished] && !self.YAMLError) {
        NSError * error;

        if ([self.responseData length] == 0 || [self.responseString isEqualToString:@" "]) {
            self.responseYAML = nil;
        } else {
            NSData *YAMLData = self.responseData;
            self.responseYAML = [YAMLSerialization YAMLWithData:YAMLData options:kYAMLReadOptionStringScalars error:&error];
        }

        self.YAMLError = error;
    }
    [self.lock unlock];
    return _responseYAML;
}

- (void)setCompletionBlockWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
    self.completionBlock = ^ {
        if (self.error) {
            if (failure) {
                dispatch_async(self.failureCallbackQueue ?: dispatch_get_main_queue(), ^{
                    failure(self, self.error);
                });
            }
        } else {
            NSArray * yaml = self.responseYAML;

            if (self.YAMLError) {
                if (failure) {
                    dispatch_async(self.failureCallbackQueue ?: dispatch_get_main_queue(), ^{
                        failure(self, self.YAMLError);
                    });
                }
            } else {
                if (success) {
                    dispatch_async(self.successCallbackQueue ?: dispatch_get_main_queue(), ^{
                        success(self, yaml);
                    });
                }
            }
        }
    };
#pragma clang diagnostic pop
}

@end