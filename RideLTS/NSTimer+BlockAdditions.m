//
//  NSTimer+BlockAdditions.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "NSTimer+BlockAdditions.h"

@interface NSTimerBlock : NSObject
@property (nonatomic, copy) VoidBlock block;
- (void) fire:(NSTimer *)timer;
- (id) initWithBlock:(VoidBlock)block;
+ (NSTimerBlock *) objForBlock:(VoidBlock)block;
@end

@implementation NSTimer (BlockAdditions)

+ (NSTimer *) timerWithTimeInterval:(NSTimeInterval)ti block:(VoidBlock)block repeats:(BOOL)yesOrNo {
    NSTimerBlock * timerBlock = [NSTimerBlock objForBlock:block];
    return [NSTimer timerWithTimeInterval:ti target:timerBlock selector:@selector(fire:) userInfo:nil repeats:yesOrNo];
}

+ (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)ti block:(VoidBlock)block repeats:(BOOL)yesOrNo {
    NSTimer * timer = [NSTimer timerWithTimeInterval:ti block:block repeats:yesOrNo];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    return timer;
}

@end

@implementation NSTimerBlock

@synthesize block = _block;

+ (NSTimerBlock *) objForBlock:(VoidBlock)block {
    return [[NSTimerBlock alloc] initWithBlock:block];
}

- (id) initWithBlock:(VoidBlock)block {
    self = [self init];
    if (!self) return nil;
    _block = [block copy];
    return self;
}

- (void) fire:(NSTimer *)timer {
    if (self.block) self.block();
}

@end