//
//  NSString+LTSNumberFormat.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (LTSNumberFormat)

- (NSNumber *) number;

@end
