//
//  NSTimer+BlockAdditions.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^VoidBlock)(void);

@interface NSTimer (BlockAdditions)

+ (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)ti block:(VoidBlock)block repeats:(BOOL)yesOrNo;
+ (NSTimer *) timerWithTimeInterval:(NSTimeInterval)ti block:(VoidBlock)block repeats:(BOOL)yesOrNo;

@end
