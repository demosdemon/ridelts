//
//  NSString+LTSNumberFormat.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "NSString+LTSNumberFormat.h"

@implementation NSObject (LTSNumberFormat)

- (NSNumber *) number {
    if ([self isKindOfClass:[NSNumber class]]) return (NSNumber *)self;
    if (![self isKindOfClass:[NSString class]]) return [NSNumber numberWithBool:NO];

    if ([(NSString *)self caseInsensitiveCompare:@"true"] == NSOrderedSame)
        return [NSNumber numberWithBool:YES];

    if ([(NSString *)self caseInsensitiveCompare:@"false"] == NSOrderedSame)
        return [NSNumber numberWithBool:NO];

    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterDecimalStyle];
    return [nf numberFromString:(NSString *)self];
}

@end
