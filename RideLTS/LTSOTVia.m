//
//  RLTSOTVia.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/8/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "LTSOTVia.h"
#import "LTSOTViaRequestOperation.h"

static NSOperationQueue * _OTViaQueue;
static NSOperationQueue * OTViaQueue() {
    if (_OTViaQueue) return _OTViaQueue;
    _OTViaQueue = [[NSOperationQueue alloc] init];
    _OTViaQueue.name = @"RLTSOTVia Operation Queue";
    _OTViaQueue.maxConcurrentOperationCount = 1;
    return _OTViaQueue;
}

static AFHTTPClient * _OTViaClient;
static AFHTTPClient * OTViaClient() {
    if (_OTViaClient) return _OTViaClient;
    _OTViaClient = [AFHTTPClient clientWithBaseURL:BASE_URL];
    _OTViaClient.operationQueue.maxConcurrentOperationCount = 1;
    return _OTViaClient;
}

@implementation LTSOTVia

@synthesize delegate = _delegate;

- (id) init {
    if (self = [super init]) {
        _lastVehicleHttpRequestTime = [NSNumber numberWithInt:0];
        _lastShelterHttpRequestTime = [NSNumber numberWithInt:0];

    }
    return self;
}

- (void) dealloc {
    [self stopTimers];
}

- (void) startTimers {
    if (!_statusTimer)
        _statusTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(fireStatusTimer:) userInfo:nil repeats:YES];
}

- (void) stopTimers {
    if (_statusTimer) {
        [_statusTimer invalidate];
        _statusTimer = nil;
    }
}

- (void) fireStatusTimer:(NSTimer *)timer {

}

- (void) requestRoutes {
    NSURL * localPlist = [[NSBundle mainBundle] URLForResource:@"route" withExtension:@"plist"];
    if (localPlist) {
        [[[NSDictionary dictionaryWithContentsOfURL:localPlist] objectForKey:@"RouteArray"] enumerateObjectsUsingBlock:^(NSDictionary * route, NSUInteger idx, BOOL *stop) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if ([self.delegate respondsToSelector:@selector(updateRoute:)]) {
                    [self.delegate updateRoute:[route objectForKey:@"Route"]];
                }
            }];
        }];

        return;
    }

    NSURLRequest * request = [OTViaClient() requestWithMethod:@"GET" path:kRouteUrl parameters:nil];
    LTSOTViaRequestOperation * op = [LTSOTViaRequestOperation YAMLRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSArray *YAML) {
        if (YAML.count == 0) return;
        if (![self.delegate respondsToSelector:@selector(updateRoute:)]) return;

        NSDictionary * doc = [YAML objectAtIndex:0];
        NSArray * routeArray = [doc objectForKey:@"RouteArray"];
        [routeArray enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
            [self.delegate updateRoute:[obj objectForKey:@"route"]];
        }];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSArray *JSON) {
        NSLog(@"%@\n", error);
        NSLog(@"%@\n", response);
    }];

    [OTViaClient() enqueueHTTPRequestOperation:op];
}

- (void) requestKMLForRoute:(NSNumber *)route {
    if (!route) return;

    NSURL * localUrl = [[NSBundle mainBundle] URLForResource:[route stringValue] withExtension:@"kml"];
    if (localUrl) {
        [self.delegate parseKML:[NSData dataWithContentsOfURL:localUrl] forRoute:route];
        return;
    }

    // short url
    // long url
//    NSURLRequest * request = [OTViaClient() requestWithMethod:@"GET" path:kRouteUrl parameters:nil];
//    LTSOTViaRequestOperation * op = [LTSOTViaRequestOperation YAMLRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSArray *YAML) {
//        if (YAML.count == 0) return;
//        if (![self.delegate respondsToSelector:@selector(updateRoute:)]) return;
//
//        NSDictionary * doc = [YAML objectAtIndex:0];
//        NSArray * routeArray = [doc objectForKey:@"RouteArray"];
//        [routeArray enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
//            [self.delegate updateRoute:obj];
//        }];
//    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSArray *JSON) {
//        NSLog(@"%@\n", error);
//    }];
//
//    [OTViaClient() enqueueHTTPRequestOperation:op];

}

- (void) requestVehiclesForRoutes:(NSArray *)routes {
    if (NSNilOrEmpty(routes)) return;

    NSString * path = [NSString stringWithFormat:kVehicleUrlFormat,
                       [routes componentsJoinedByString:@","],
                       _lastVehicleHttpRequestTime,
                       _lastVehicleHttpRequestTime.boolValue ? @"false" : @"true"];

    NSURLRequest * request = [OTViaClient() requestWithMethod:@"GET" path:path parameters:nil];
    LTSOTViaRequestOperation * op = [LTSOTViaRequestOperation YAMLRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSArray *YAML) {
        if (YAML.count == 0) return;
        if (![self.delegate respondsToSelector:@selector(updateVehicle:)]) return;

        NSDictionary * doc = [YAML objectAtIndex:0];
        NSArray * routeArray = [doc objectForKey:@"VehicleArray"];
        [routeArray enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
            [self.delegate updateVehicle:[obj objectForKey:@"vehicle"]];
        }];

        _lastVehicleHttpRequestTime = [[doc objectForKey:@"lastUpdateTime"] number];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSArray *JSON) {
        NSLog(@"%@\n", error);
    }];

    [OTViaClient() enqueueHTTPRequestOperation:op];

}

- (void) requestSheltersForRoutes:(NSArray *)routes {
    if (NSNilOrEmpty(routes)) return;

    NSString * path = [NSString stringWithFormat:kShelterUrlFormat,
                       [routes componentsJoinedByString:@","],
                       _lastShelterHttpRequestTime,
                       _lastShelterHttpRequestTime.boolValue ? @"true" : @"false"];

    NSURLRequest * request = [OTViaClient() requestWithMethod:@"GET" path:path parameters:nil];
    LTSOTViaRequestOperation * op = [LTSOTViaRequestOperation YAMLRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSArray *YAML) {
        if (YAML.count == 0) return;
        if (![self.delegate respondsToSelector:@selector(updateShelter:)]) return;

        NSDictionary * doc = [YAML objectAtIndex:0];
        NSArray * routeArray = [doc objectForKey:@"VehicleArray"];
        [routeArray enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
            [self.delegate updateShelter:[obj objectForKey:@"shelter"]];
        }];

        _lastShelterHttpRequestTime = [[doc objectForKey:@"lastUpdateTime"] number];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSArray *JSON) {
        NSLog(@"%@\n", error);
    }];

    [OTViaClient() enqueueHTTPRequestOperation:op];
}

@end

