//
//  main.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/8/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LTSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LTSAppDelegate class]));
    }
}
