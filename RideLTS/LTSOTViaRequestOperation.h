//
//  LTSOTViaRequestOperation.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "AFHTTPRequestOperation.h"

typedef void(^YAMLRequestSuccessBlock)(NSURLRequest * request, NSHTTPURLResponse * response, NSArray * YAML);
typedef void(^YAMLRequestFailureBlock)(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error, NSArray * JSON);

@interface LTSOTViaRequestOperation : AFHTTPRequestOperation

@property (readonly, nonatomic, strong) NSArray * responseYAML;

+ (LTSOTViaRequestOperation *) YAMLRequestOperationWithRequest:(NSURLRequest *)urlRequest
                                                       success:(YAMLRequestSuccessBlock)success
                                                       failure:(YAMLRequestFailureBlock)failure;



@end
