//
//  LTSVehicleView.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "LTSVehicleView.h"

@implementation LTSVehicleView

@synthesize mapView = _mapView;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithAnnotation:(LTSVehicleAnnotation *)annotation mapView:(MKMapView *)mapview {
    if (self = [super initWithAnnotation:annotation reuseIdentifier:[[self class] ReuseIdentifier]]) {
        self.canShowCallout = YES;
        self.mapView = mapview;
    }
    return self;
}

- (void) setAnnotation:(id<MKAnnotation>)annotation {
    if (annotation)
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super setAnnotation:annotation];
    
    if (!annotation) return;
    if (![annotation isKindOfClass:[LTSVehicleAnnotation class]]) return;

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(updateVehicleCoordinate:)
//                                                 name:kUpdateCoordinateNotification
//                                               object:annotation];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateVehicleAngle:)
                                                 name:kUpdateAngleNotification
                                               object:annotation];
    NSUInteger angle = [(LTSVehicleAnnotation*)annotation angle];
    angle = LTSRoundToStep(angle, (360/8));
    NSString * imageName = [NSString stringWithFormat:@"vehicle-direction-%03u.png", angle];
    UIImage * image = [UIImage imageNamed:imageName];
    if (!image)
        NSLog(@"Error: Cannot find image %@", imageName);
    else
        self.image = image;
}

- (void) updateVehicleAngle:(NSNotification *)notification {
    NSUInteger angle = [(LTSVehicleAnnotation*)self.annotation angle];
    angle = LTSRoundToStep(angle, (360/8));
    NSString * imageName = [NSString stringWithFormat:@"vehicle-direction-%03u.png", angle];
    UIImage * image = [UIImage imageNamed:imageName];
    if (!image)
        NSLog(@"Error: Cannot find image %@", imageName);
    else
        self.image = image;
}

//- (void) updateVehicleCoordinate:(NSNotification *)notification {
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        NSUInteger angle = [(LTSVehicleAnnotation*)self.annotation angle];
//        angle = LTSRoundToStep(angle, (360/8));
//        NSString * imageName = [NSString stringWithFormat:@"vehicle-direction-%03u.png", angle];
//        UIImage * image = [UIImage imageNamed:imageName];
//        if (!image)
//            NSLog(@"Error: Cannot find image %@", imageName);
//        else
//            self.image = image;
//        
//        MKMapPoint mapPoint = MKMapPointForCoordinate([(LTSVehicleAnnotation*)[notification object] coordinate]);
//        if (MKMapRectContainsPoint(self.mapView.visibleMapRect, mapPoint)) {
//            CGPoint moveTo;
//            CGFloat zoomFactor =  self.mapView.visibleMapRect.size.width / self.mapView.bounds.size.width;
//            moveTo.x = mapPoint.x/zoomFactor;
//            moveTo.y = mapPoint.y/zoomFactor;
//            [UIView animateWithDuration:1.0 animations:^{
//                self.center = moveTo;
//            }];
//        }
//    }];
//}

+ (NSString *) ReuseIdentifier {
    return @"LTSVehicleView";
}

+ (MKAnnotationView *) viewWithAnnotation:(LTSVehicleAnnotation *)annotation mapView:(MKMapView *)mapview {
    return [[LTSVehicleView alloc] initWithAnnotation:annotation mapView:mapview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
