//
//  LTSRouteOverlay.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LTSRouteOverlay : NSObject

@property (readonly, nonatomic, strong) NSNumber * RouteID;
@property (readonly, nonatomic, strong) NSNumber * RouteNum;
@property (readonly, nonatomic, copy) NSString * Name;
@property (readonly, nonatomic, strong) UIColor * ColorRoute;
@property (readonly, nonatomic, strong) UIColor * ColorVehicle;

- (void) parseKML:(NSData *)data;
@property (nonatomic, readonly) NSArray *overlays;
- (MKOverlayView *)viewForOverlay:(id <MKOverlay>)overlay;

// Keys that must be present:
// RouteID     - Route ID - data comes in with a unique id that doesn't pertain
//             - to the real route number
// LogNum      - Real Route Number
// Location    - NSValue with CLLocationCoordinate2D object
//               If not present then will look for a CVLocation dictionary
// Angle       - NSNumber with a 0 <= n < 360. If not present will look for CVLocation
// CVLocaation - If Location is not present. {vehicleID : 7112, latitude :
//               3017416, longitude : -9207470, angle : 129, locStatus : 1,
//               locTime : 1360426415, speed : 0, bValidSanp : false, snapLat :
//               0, snapLong : 0, snapAngle : 0}
// Optional:
// oos         - Marks vehicle as out of service and will not be shown on the map

+ (LTSRouteOverlay *) routeWithDictionary:(NSDictionary *)dict;

@end
