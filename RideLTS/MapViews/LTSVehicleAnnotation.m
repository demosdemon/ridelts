//
//  LTSVehicleData.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "LTSVehicleAnnotation.h"

static NSMutableDictionary * _VehicleCache;
static NSMutableDictionary * VehicleCache() {
    if (!_VehicleCache)
        _VehicleCache = [NSMutableDictionary dictionary];
    return _VehicleCache;
}

@interface LTSVehicleAnnotation ()
@property (nonatomic, strong, readwrite) NSNumber * RouteID;
@property (nonatomic, assign, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign, readwrite) NSUInteger angle;
@end

@implementation LTSVehicleAnnotation

@synthesize VehicleID  = _VehicleID;
@synthesize RouteID    = _RouteID;
@synthesize OOS        = _OOS;
@synthesize coordinate = _coordinate;
@synthesize angle      = _angle;

- (id) initWithID:(NSNumber *)_id {
    if (self = [self init]) {
        _VehicleID = _id;
    }
    return self;
}

- (NSString *) title {
    return [NSString stringWithFormat:@"Route %@", self.RouteID];
}

- (NSString *) subtitle {
    return [NSString stringWithFormat:@"Bus %@", self.VehicleID];
}

- (void) setAngle:(NSUInteger)angle {
    [self willChangeValueForKey:@"angle"];
    _angle = angle % 360;
    [self willChangeValueForKey:@"angle"];

    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateAngleNotification object:self];
}

- (void) setCoordinate:(CLLocationCoordinate2D)coordinate {
    [self willChangeValueForKey:@"coordinate"];
    _coordinate = coordinate;
    [self didChangeValueForKey:@"coordinate"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateCoordinateNotification object:self];
}

+ (LTSVehicleAnnotation *) vehicleWithDictionary:(NSDictionary *)dict {
    NSNumber * vehicleId = [[dict objectForKey:@"id"] number];

    LTSVehicleAnnotation * vehicle = [VehicleCache() objectForKey:vehicleId];
    if (!vehicle) vehicle = [[LTSVehicleAnnotation alloc] initWithID:vehicleId];

    [dict enumerateKeysAndObjectsUsingBlock:^(NSString * key, id obj, BOOL *stop) {
        if (kMATCH(routeid)) {
            vehicle.RouteID = [obj number];
        } else if (kMATCH(oos)) {
            vehicle.OOS = [[obj number] boolValue];
        } else if (kMATCH(CVlocation)) {
            NSDictionary * cvloc = (NSDictionary *)obj;
            double latitude = [[[cvloc objectForKey:@"latitude"] number] doubleValue] / 100000.0;
            double longitude = [[[cvloc objectForKey:@"longitude"] number] doubleValue] / 100000.0;
            vehicle.coordinate = CLLocationCoordinate2DMake(latitude, longitude);

            vehicle.angle = [[[cvloc objectForKey:@"angle"] number] unsignedIntegerValue];
        }
    }];

    [VehicleCache() setObject:vehicle forKey:vehicleId];

    return vehicle;
}

- (BOOL) isEqual:(id)object {
    return [object isKindOfClass:[LTSVehicleAnnotation class]] && [self.VehicleID isEqual:[object VehicleID]];
}

@end
