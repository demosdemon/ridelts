//
//  LTSRouteOverlay.m
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import "LTSRouteOverlay.h"
#import "KMLParser.h"

static NSOperationQueue * _ParserQueue;
static NSOperationQueue * ParserQueue() {
    if (!_ParserQueue) {
        _ParserQueue = [[NSOperationQueue alloc] init];
    }
    return _ParserQueue;
}

static NSMutableDictionary * _RouteCache;
static NSMutableDictionary * RouteCache() {
    if (!_RouteCache)
        _RouteCache = [NSMutableDictionary dictionary];
    return _RouteCache;
}

@interface LTSRouteOverlay ()
@property (readwrite, nonatomic, strong) NSNumber * RouteNum;
@property (readwrite, nonatomic, copy) NSString * Name;
@property (readwrite, nonatomic, strong) UIColor * ColorRoute;
@property (readwrite, nonatomic, strong) UIColor * ColorVehicle;
@property (readwrite, nonatomic, strong) KMLParser * kmlparser;
@property (readwrite, nonatomic, strong) NSRecursiveLock * lock;
@end

@implementation LTSRouteOverlay

@synthesize RouteID = _RouteID;
@synthesize RouteNum = _RouteNum;
@synthesize Name = _Name;
@synthesize ColorRoute = _ColorRoute;
@synthesize ColorVehicle = _ColorVehicle;
@synthesize kmlparser = _kmlparser;
@synthesize lock = _lock;

- (id) initWithID:(NSNumber *)_id {
    if (self = [self init]) {
        _RouteID = _id;
        _lock = [[NSRecursiveLock alloc] init];
    }
    return self;
}

- (void) parseKML:(NSData *)data {
    _kmlparser = [[KMLParser alloc] initWithData:data];
    [_kmlparser parseKML];
}

- (NSArray *) overlays {
    return self.kmlparser.overlays;
}

- (MKOverlayView *)viewForOverlay:(id <MKOverlay>)overlay {
    return [self.kmlparser viewForOverlay:overlay];
}

+ (LTSRouteOverlay *) routeWithDictionary:(NSDictionary *)dict {
    NSNumber * routeID = [[dict objectForKey:@"RouteID"] number];
    LTSRouteOverlay * route = [RouteCache() objectForKey:routeID];
    if (!route) route = [[LTSRouteOverlay alloc] initWithID:routeID];

    [dict enumerateKeysAndObjectsUsingBlock:^(NSString * key, id obj, BOOL *stop) {
        if (kMATCH(Name)) {
            route.Name = obj;
        } else if (kMATCH(ColorRoute)) {
            route.ColorRoute = [UIColor colorWithKMLString:obj];
        } else if (kMATCH(ColorVehicle)) {
            route.ColorVehicle = [UIColor colorWithKMLString:obj];
        } else if (kMATCH(LogNum)) {
            route.RouteNum = [obj number];
        }
    }];

    [RouteCache() setObject:route forKey:routeID];

    return route;
}

@end
