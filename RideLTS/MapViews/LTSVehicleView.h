//
//  LTSVehicleView.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "LTSVehicleAnnotation.h"

@interface LTSVehicleView : MKAnnotationView

@property (weak, nonatomic) MKMapView * mapView;

+ (NSString *) ReuseIdentifier;
+ (MKAnnotationView *) viewWithAnnotation:(LTSVehicleAnnotation *)annotation mapView:(MKMapView *)mapview;

@end
