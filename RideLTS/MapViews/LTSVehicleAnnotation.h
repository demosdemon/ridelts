//
//  LTSVehicleData.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/9/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUpdateCoordinateNotification @"UpdateCoordinateNotification"
#define kUpdateAngleNotification @"UpdateAngleNotification"

@interface LTSVehicleAnnotation : NSObject <MKAnnotation>

@property (nonatomic, strong, readonly) NSNumber * VehicleID;
@property (nonatomic, strong, readonly) NSNumber * RouteID;
@property (nonatomic, assign, getter = isOOS) BOOL OOS;

@property (nonatomic, assign, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign, readonly) NSUInteger angle;

@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;

// Keys that must be present:
// id          - Unique Vehicle ID, if it matches a previously instantianated
//               vehicle, the old one will be updated and returned
// RouteID     - Route ID for the current route this bus is on. Data comes in with
//               the real route number, does not reflect LTSRouteOverlay RouteID.
// Location    - NSValue with CLLocationCoordinate2D object
//               If not present then will look for a CVLocation dictionary
// Angle       - NSNumber with a 0 <= n < 360. If not present will look for CVLocation
// CVLocaation - If Location is not present. {vehicleID : 7112, latitude :
//               3017416, longitude : -9207470, angle : 129, locStatus : 1,
//               locTime : 1360426415, speed : 0, bValidSanp : false, snapLat :
//               0, snapLong : 0, snapAngle : 0}
// Optional:
// oos         - Marks vehicle as out of service and will not be shown on the map
+ (LTSVehicleAnnotation *) vehicleWithDictionary:(NSDictionary *)dict;

@end
