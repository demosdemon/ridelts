//
//  LTSUtilities.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/10/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MKCoordinateSpanForMiles(mi) {(mi)/69.0, (mi)/69.0}
#define NSAppDelegate ((LTSAppDelegate *)[[UIApplication sharedApplication] delegate])
#define NSNilOrEmpty(array) (((array) == nil) || ([(array) count] == 0))
#define NSNotNilNorEmpty(array) (!(NSNilOrEmpty((array))))
#define LTSRoundToStep(num, step) ((step) * floor(((num)/(step)) + 0.5))

#define kMATCH(sKey) ([key caseInsensitiveCompare:@#sKey] == NSOrderedSame)

#define kBaseUrl   @"http://lafayette.otvia.com"
#define BASE_URL [NSURL URLWithString:kBaseUrl]

#define kRouteUrl  @"packet/json/route"
#define kStatusUrl @"packet/json/status"

#define kVehicleUrlFormat @"packet/json/vehicle?routes=%@&lastVehicleHttpRequestTime=%@&forceUpdate=%@"
#define kShelterUrlFormat @"packet/json/shelter?routes=%@&lastVehicleHttpRequestTime=%@&forceUpdate=%@"
#define kRouteLayerFormat @"packet/json/routelayer?%@"

#define kKmlShortenUrlFormat @"kml/shortenURL?routes=%@"
#define kKmlUrlFormat        @"kml/routeKey?%@"

@interface LTSUtilities : NSObject

@end
