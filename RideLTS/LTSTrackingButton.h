//
//  LTSTrackingButton.h
//  RideLTS
//
//  Created by Joachim LeBlanc on 2/10/13.
//  Copyright (c) 2013 Joachim LeBlanc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LTSTrackingButton : UIImageView

@property (nonatomic, retain) MKMapView * MapView;
@property (nonatomic, assign) MKUserTrackingMode TrackingMode;

- (id)initWithMapView:(MKMapView *)mapView;

@end
